import time
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)

RED = 3
GREEN = 5
BLUE = 8

GPIO.setup(3, GPIO.OUT)
GPIO.setup(5, GPIO.OUT)
GPIO.setup(8, GPIO.OUT)

def off():
 GPIO.output(RED, False)
 GPIO.output(GREEN, False)
 GPIO.output(BLUE, False)

off()
